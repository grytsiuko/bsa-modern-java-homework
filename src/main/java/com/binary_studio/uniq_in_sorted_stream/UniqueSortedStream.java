package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {

		// Local variable should be (effectively) final to be used in lambda, so we can't
		// use Row<T> last
		//
		// We can't declare static or instance field because of Generic in the method, not
		// in the class
		//
		// So we create an anonymous class-wrapper for the last element

		final var last = new Object() {
			Row<T> row;

		};

		return stream.filter(row -> {
			boolean isNew = (last.row == null || !row.getPrimaryId().equals(last.row.getPrimaryId()));
			last.row = row;
			return isNew;
		});
	}

}
