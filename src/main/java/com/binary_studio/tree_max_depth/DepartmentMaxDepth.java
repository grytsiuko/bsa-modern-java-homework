package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		Queue<Department> queue = new LinkedList<>();
		addDepartmentIfNotNull(queue, rootDepartment);

		Integer depth = 0;
		while (!queue.isEmpty()) {
			processDepartmentsLayer(queue);
			depth++;
		}

		return depth;
	}

	private static void addDepartmentIfNotNull(Queue<Department> queue, Department department) {
		Optional<Department> departmentOptional = Optional.ofNullable(department);
		departmentOptional.ifPresent(queue::add);
	}

	private static void processDepartmentsLayer(Queue<Department> queue) {
		int currentLayerSize = queue.size();

		for (int i = 0; i < currentLayerSize; i++) {
			processDepartment(queue, queue.element());
			queue.poll();
		}
	}

	private static void processDepartment(Queue<Department> queue, Department department) {
		for (Department subDepartment : department.subDepartments) {
			addDepartmentIfNotNull(queue, subDepartment);
		}
	}

}
