package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldMaxHP;

	private PositiveInteger shieldLeftHP;

	private PositiveInteger hullMaxHP;

	private PositiveInteger hullLeftHP;

	private PositiveInteger capacitorMaxVolume;

	private PositiveInteger capacitorLeftVolume;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitorAmount,
			PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size,
			AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {

		this.name = name;
		this.shieldMaxHP = shieldHP;
		this.shieldLeftHP = shieldHP;
		this.hullMaxHP = hullHP;
		this.hullLeftHP = hullHP;
		this.capacitorMaxVolume = capacitorAmount;
		this.capacitorLeftVolume = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		this.capacitorLeftVolume = PositiveInteger.add(this.capacitorLeftVolume, this.capacitorRechargeRate);
		if (this.capacitorLeftVolume.compareTo(this.capacitorMaxVolume) > 0) {
			this.capacitorLeftVolume = this.capacitorMaxVolume;
		}
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		PositiveInteger neededCapacitor = this.attackSubsystem.getCapacitorConsumption();
		if (neededCapacitor.compareTo(this.capacitorLeftVolume) > 0) {
			return Optional.empty();
		}

		this.capacitorLeftVolume = PositiveInteger.subtract(this.capacitorLeftVolume, neededCapacitor);
		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		PositiveInteger damageLeft = reducedAttack.damage;

		damageLeft = applyAttackToShield(damageLeft);
		damageLeft = applyAttackToHull(damageLeft);

		if (damageLeft.value() == 0) {
			return new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, this);
		}
		else {
			return new AttackResult.Destroyed();
		}
	}

	private PositiveInteger applyAttackToShield(PositiveInteger damage) {
		PositiveInteger appliedDamage = PositiveInteger.min(this.shieldLeftHP, damage);
		this.shieldLeftHP = PositiveInteger.subtract(this.shieldLeftHP, appliedDamage);

		return PositiveInteger.subtract(damage, appliedDamage);
	}

	private PositiveInteger applyAttackToHull(PositiveInteger damage) {
		PositiveInteger appliedDamage = PositiveInteger.min(this.hullLeftHP, damage);
		this.hullLeftHP = PositiveInteger.subtract(this.hullLeftHP, appliedDamage);

		return PositiveInteger.subtract(damage, appliedDamage);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		PositiveInteger neededCapacitor = this.defenciveSubsystem.getCapacitorConsumption();
		if (neededCapacitor.compareTo(this.capacitorLeftVolume) > 0) {
			return Optional.empty();
		}

		this.capacitorLeftVolume = PositiveInteger.subtract(this.capacitorLeftVolume, neededCapacitor);
		return regenerateAll();
	}

	private Optional<RegenerateAction> regenerateAll() {
		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();

		PositiveInteger shieldHPRegenerated = regenerateShield(regenerateAction.shieldHPRegenerated);
		PositiveInteger hullHPRegenerated = regenerateHull(regenerateAction.hullHPRegenerated);

		return Optional.of(new RegenerateAction(shieldHPRegenerated, hullHPRegenerated));
	}

	private PositiveInteger regenerateShield(PositiveInteger amountHP) {
		PositiveInteger availableHP = PositiveInteger.subtract(this.shieldMaxHP, this.shieldLeftHP);
		PositiveInteger regeneratedHP = PositiveInteger.min(amountHP, availableHP);

		this.shieldLeftHP = PositiveInteger.add(this.shieldLeftHP, regeneratedHP);
		return regeneratedHP;
	}

	private PositiveInteger regenerateHull(PositiveInteger amountHP) {
		PositiveInteger availableHP = PositiveInteger.subtract(this.hullMaxHP, this.hullLeftHP);
		PositiveInteger regeneratedHP = PositiveInteger.min(amountHP, availableHP);

		this.hullLeftHP = PositiveInteger.add(this.hullLeftHP, regeneratedHP);
		return regeneratedHP;
	}

}
