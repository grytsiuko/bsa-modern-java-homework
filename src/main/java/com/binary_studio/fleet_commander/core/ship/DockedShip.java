package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {

		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = null;
		this.defenciveSubsystem = null;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		Integer leftPowergridAfterFitting = getLeftPowergridAfterFitting(this.attackSubsystem, subsystem);

		if (leftPowergridAfterFitting < 0) {
			throw new InsufficientPowergridException(-leftPowergridAfterFitting);
		}

		this.powergridOutput = PositiveInteger.of(leftPowergridAfterFitting);
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		Integer leftPowergridAfterFitting = getLeftPowergridAfterFitting(this.defenciveSubsystem, subsystem);

		if (leftPowergridAfterFitting < 0) {
			throw new InsufficientPowergridException(-leftPowergridAfterFitting);
		}

		this.powergridOutput = PositiveInteger.of(leftPowergridAfterFitting);
		this.defenciveSubsystem = subsystem;
	}

	private Integer getLeftPowergridAfterFitting(Subsystem oldSubsystem, Subsystem newSubsystem) {
		Integer oldPowergrid = getSubsystemPowergridConsumption(oldSubsystem);
		Integer newPowergrid = getSubsystemPowergridConsumption(newSubsystem);

		Integer availablePowergrid = this.powergridOutput.value() + oldPowergrid;
		return availablePowergrid - newPowergrid;
	}

	private Integer getSubsystemPowergridConsumption(Subsystem subsystem) {
		Optional<Subsystem> subsystemOptional = Optional.ofNullable(subsystem);
		return subsystemOptional.map(Subsystem::getPowerGridConsumption).map(PositiveInteger::value).orElse(0);
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem != null && this.attackSubsystem != null) {
			return createCombatReadyShip();
		}
		if (this.defenciveSubsystem != null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.attackSubsystem != null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		throw NotAllSubsystemsFitted.bothMissing();
	}

	private CombatReadyShip createCombatReadyShip() {
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitorAmount,
				this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
	}

}
