package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import static java.lang.Double.min;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {

		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = getSizeReductionModifier(target);
		double speedReductionModifier = getSpeedReductionModifier(target);

		double damage = getDamage(sizeReductionModifier, speedReductionModifier);
		return PositiveInteger.of((int) Math.ceil(damage));
	}

	private double getSizeReductionModifier(Attackable target) {
		return target.getSize().compareTo(this.optimalSize) >= 0 ? 1.
				: 1. * target.getSize().value() / this.optimalSize.value();
	}

	private double getSpeedReductionModifier(Attackable target) {
		return target.getCurrentSpeed().compareTo(this.optimalSpeed) <= 0 ? 1.
				: 1. * this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
	}

	private double getDamage(double sizeReductionModifier, double speedReductionModifier) {
		return this.baseDamage.value() * min(sizeReductionModifier, speedReductionModifier);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
